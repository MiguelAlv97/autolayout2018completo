//
//  TapViewController.swift
//  AutoLayout2018
//
//  Created by Miguel Esteban Alvarez Naranjo on 27/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func TapGestureActivated(_ sender: Any) { // al gesture tap se le puede conectar como outlet o action, otutlet nos permite configurar los tap
        
        mainView.backgroundColor = .green
    }
    

}
