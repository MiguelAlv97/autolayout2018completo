//
//  PinchViewController.swift
//  AutoLayout2018
//
//  Created by Miguel Esteban Alvarez Naranjo on 27/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    @IBOutlet weak var heightC: NSLayoutConstraint!
    
    @IBOutlet weak var widthC: NSLayoutConstraint!
    
    var originalHeight:CGFloat = 0
    var originalWidth:CGFloat = 0
    
    @IBOutlet weak var squareView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        originalHeight = heightC.constant
        originalWidth = widthC.constant
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func PinchG(_ sender: UIPinchGestureRecognizer) {
        //let pinch = sender as! UIPinchGestureRecognizer
        
        print ("scale:  \(sender.scale)")
        print ("velocity: \(sender.velocity)")
        
        heightC.constant = originalHeight * sender.scale
        widthC.constant = originalWidth * sender.scale
        
        if sender.state == .ended {
            UIView.animate(withDuration: 0.3){
                self.heightC.constant = self.originalHeight
                self.widthC.constant = self.originalWidth
            }
        }
    }
    
    
    @IBAction func LongPressActivated(_ sender: Any) {
        
        squareView.backgroundColor = .red
        
    }
    
    
    @IBAction func RotationActivated(_ sender: UIRotationGestureRecognizer) {
        
        guard sender.view != nil else {return}
        
        if sender.state == .began || sender.state == .changed{
            
            sender.view?.transform = sender.view!.transform.rotated(by: sender.rotation)
            sender.rotation = 0
            
        }
//        if sender.state == .ended{
//
//            UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWhitDamping: 0.6, initialSpringVelocity: 1, option: .curveEaseOut ,
//                           animation:{
//
//                            sender.view?.transform = CGAffineTransform(rotationAngle: 0.0))
//
//            }, complation: { _ in sender.rotation = 0})
//
//        }
        if sender.state == .ended{
            
            UIView.animate(withDuration: 0.3, delay:0.1, animations: {
                sender.view?.transform = CGAffineTransform(rotationAngle: 0.0)
            })
            
        }
        
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
