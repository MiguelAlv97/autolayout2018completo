//
//  ColoresViewController.swift
//  AutoLayout2018
//
//  Created by Miguel Esteban Alvarez Naranjo on 4/7/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class ColoresViewController: UIViewController {

    
    @IBOutlet weak var RojoView: UIView!
    
    @IBOutlet weak var CelesteView: UIView!
    @IBOutlet weak var VerdeView: UIView!
    @IBOutlet weak var AzulView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func TapRojo(_ sender: Any) {
        RojoView.backgroundColor = .blue
    }
    
    @IBAction func TapAzul(_ sender: Any) {
    AzulView.backgroundColor = .black
    
    }
    
    @IBAction func TapVerde(_ sender: Any) {
        VerdeView.backgroundColor = .red
    }
    
    @IBAction func Tapceleste(_ sender: Any) {
        CelesteView.backgroundColor = .green
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
